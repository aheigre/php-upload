<?php
/**
 * Upload-script!
 * Copyright @ 2011 Atle Heigre <atlhei@malm.in>
 */

// we need the core!
require("static/core.php");

// We only allow users with access level 1
//core::$s->user->access(1);


if (isset($_POST["upload"]))
{
	try {
		core::$s->upload->start($_FILES["file"], $_POST["upload"]);
		
		//core::$s->upload->prepare();
		//core::$s->upload->commit();
	}
	
	catch (Exception $e) {
		die('Error: ' . $e->getMessage());
	}
}

// awfahwfh
if (isset($_GET["files"]))
{
	foreach (core::$c->get_folders() as $folder)
	{
		$path = FILES . $folder;
		
		// Open the folder
		$dir_handle = @opendir($path) or die("Unable to open $path");
		
		// Loop through the files
		while ($file = readdir($dir_handle)) 
		{
			if($file == "." || $file == ".." || $file == "index.php" || $file == "style.css" || $file == "img") continue;
			
			$files[$folder] = $file;
		}
		
		// Close
		closedir($dir_handle);
	}
	
	pre_dump($files);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Upload :: Eizio.net</title>
	</head>
	
	<body>
		<form action="" method="post" enctype="multipart/form-data">
			<label for="file">File:</label>
			<input type="file" name="file" id="file" /><br /><br />
			
			<label for="folder">Folder:</label>
			<select name="folder">
			<?php
				$folders = core::$c->get_folders();
				
				foreach ($folders as $folder)
				{
					$folder = trim($folder);
					$folder = ucfirst($folder);
					
					echo "
				<option name='".$folder."'>".($folder == "/" ? $folder . " (default)" : $folder)."</option>";
				}
			?>
			</select>
			
			<input type="submit" name="upload" value="UPLOAD" />
		</form>
	</body>
</html>