<?php
/**
 * Configuration options
 */
$config = new config();

class config
{
	private $settings = array (
		'upload' => array (
			'path'		=> 'http://eizio.net/',
			'folders'	=> array (
				'/', '/terror'
			)
		),
		
		'database' => array (
			'user'	=> 'root',
			'pass'	=> '',
			'db'	=> 'upload',
			'host'	=> 'localhost'
		)
	);
	
	public function get($value)
	{
		return $this->settings[$value];
	}
	
	public function get_folders()
	{
		return $this->settings["upload"]["folders"];
	}
}