<?php
error_reporting(-1);
ini_set("display_errors", "On");

/**
 * Shortcut to base()
 */
class core {
	/**
	 * System shortcut
	 */
	public static $s;
	
	/**
	 * Load configuration variables into this
	 * @var	array
	 */
	public static $c;
}

// Load config-file
require 'config.php';
global $config;

core::$c = $config;

/**
 * Starts the script
 */
core::$s = new base();

// ------------------------------

define("ROOT", dirname(__FILE__));
define("FILES", dirname(dirname(ROOT)) . "/files");

// ------------------------------


/**
 * The core-script
 */
class base {
	public $db, $upload;
	
	/**
	 * Unsets the variables used in the script
	 */
	public function __construct()
	{
		unset($this->db);
		unset($this->upload);
		unset($this->user);
	}
	
	/**
	* core::$s->(module)
	*/
	public function __get($module)
	{
		switch ($module)
		{
			/**
			* Load the database module
			*/
			case "db":
				require_once("class/db.php");
				$this->db = new db();
				return $this->db;
			break;
			
			/**
			* Load the upload module
			*/
			case "upload":
				require_once("class/upload.php");
				$this->upload = new upload();
				return $this->upload;
			break;
			
			/**
			* Load the user module
			*/
			case "user":
				require_once("class/user.php");
				$this->user = new user();
				return $this->user;
			break;
			
			/**
			* Defaults to an exception
			*/
			default:
				throw new Exception("Could not find module '$module'");
			break;
		}
	}
}

/**
 * Nice debug output
 *
function pre_dump()
{
	foreach(func_get_args() as $item)
	{
		echo '<pre>';
		var_dump($item);
		echo '</pre>';
	}
}*/
?>