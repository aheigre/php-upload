<?php
/**
 * Upload
 * 
 * An upload script with 
 * 
 * @author Atle Heigre <atlhei@malm.in>
 * @copyright 2011
 * @version 0.2
 */

//-------------------------------------------------------------

// Get the main config and stuff
require(dirname(dirname(__FILE__)) . "/core.php");

// ---------------------------------------------

// The main upload class.
class upload {
	
	/**
	 * The original name of the file.
	 * 
	 * @var filename
	 */
	private $filename;
	
	// ------------------------------
	
	/**
	 * The new name of the file.
	 * 
	 * @var new_name
	 */
	private $new_name;
	
	// ------------------------------
	
	/**
	 * The size of the file that's being uploaded.
	 * 
	 * @var size
	 */
	private $size;
	
	// ------------------------------
	
	/**
	 * The type of the file
	 *
	 * @var type
	 */
	private $type;
	
	// ------------------------------
	
	/**
	 * The folder we are uploading to
	 */
	private $folder;
	
	// ------------------------------
	
	/**
	 * Holds any erros occuring
	 * 
	 * @array error
	 */
	private $error = array();
	
	// ------------------------------
	
	/**
	 * Is the script started?
	 * 
	 * @bool init
	 */
	private $init = false;
	
	// ---------------------------------------------
	
	/**
	 * The max size of the uploaded document.
	 *
	 * @constant
	 */
	const MAX_SIZE = 5000000; // kbit?
	
	// -------------------------------------------------------
	
	/**
	 * Starts the upload, and makes sure that
	 * all the variables is set to it's correct
	 * value.
	 *
	 * @var file
	 */
	public function start($file, $data)
	{
		// First of all, clear all the values
		$this->clear();
		
		// Set all the values
		$this->filename	= $file["name"];
		$this->tmp_name	= $file["tmp_name"];
		$this->size		= $file["size"];
		$this->type		= $file["type"];
		$this->new_name	= $this->hash($this->filename);
		
		// To what folder are we uploading to?
		$this->folder	= $data["folder"];
		
		// Get the allowed files
		//$this->file = core::$s->db->get_allowed(); // <--- Not made yet
		
		// The script is started!
		$this->init = true;
	}
	
	// -------------------------------------------------------
	
	/**
	 * Prepare the script for the upload.
	 * Check whether or not the file exsist,
	 * if the size is within the allowed
	 * amount and check whether or not the
	 * file is allowed.
	 */
	public function prepare()
	{
		// Check if the init() function has been run
		if (!$this->init)
		{
			throw new Exception("init() has not been called!");
		}
		
		// Checks the max size
		if ($this->size > MAX_SIZE)
		{
			throw new Exception("File '{$this->filename}' is too big ({$this->size}). Max allowed: ".MAX_SIZE.".");
		}
		
		// Checks if the file is within the allowed file types
		if (!in_array($this->type, $this->file["allowed"]))
		{
			throw new Exception("File '{$this->filename}' is not allowed ({$this->file['allowed']})");
		}
		
		// And finally checks if the file already exsists
		if (file_exists("view/{$this->new_name}"))
		{
			throw new Exception("File '{$this->filename}' does already exist. (tmp_name: {$this->tmp_name})");
		}
	}
	
	// -------------------------------------------------------
	
	/**
	 * Uploads the file and moves it to
	 * the apponted location.
	 */
	public function commit()
	{
		// Move the uploaded file to the folder
		move_uploaded_file($this->tmp_name, $this->folder . $this->name . $this->types[$this->type]);
		
		// Redirect us to the uploaded file
		header("Location: " . core::$c->get_url() . $this->folder.$this->name . $this->types[$this->type]);
	}
	
	// -------------------------------------------------------
	
	/**
	 * Gives us a random six character long name
	 * for the uploaded file.
	 */
	private function hash($text)
	{
		$text = md5($text);
		return substr($text, 1, 6);
	}
	
	// -------------------------------------------------------
	
	/**
	 * Clears all the values used.
	 */
	private function clear()
	{
		unset($this->new_name);
		unset($this->size);
		unset($this->type);
		unset($this->error);
		unset($this->init);
	}
}
?>