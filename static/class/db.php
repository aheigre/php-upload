<?php
/**
 * Database
 * 
 * Used for interaction with the database
 *
 * @author Atle Heigre <atlhei@malm.in>
 * @copyright 2011
 * @version 0.1
 */

//-------------------------------------------------------------

// Get the settings
require ("../core.php");

// ---------------------------------------------

class db
{
	/**
	 * The link to the SQL server
	 *
	 * @bool link
	 */
	private $link;
	
	// ------------------------------
	
	/**
	 * Settings
	 * 
	 * @var settings
	 */
	private $settings;
	
	// -------------------------------------------------------
	
	/**
	 * Connect to the database
	 */
	public function connect()
	{
		// Get the settings
		$this->settings = core::$c["database"];
		
		// Connect to the database
		$this->link	= @mysql_connect($this->settings["user"], $this->settings["pass"], $this->settings["host"]);
		
		// Get the database
		$this->db	= @mysql_select_db($this->settings["db"], $this->link);
		
		// Throw an exception if anything doesn't work
		if (!$this->link OR !$this->db)
		{
			throw new Exception("Error with database (".mysql_error($this->link)."): " . mysql_error($this->link));
		}
	}
	
	// -------------------------------------------------------
	
	/**
	 * The the allowed files from the database.
	 */
	public function get_allowed()
	{
		$query = "SELECT allowed FROM settings";
		
		$result = mysql_query($query, $this->link);
		
	}
} 